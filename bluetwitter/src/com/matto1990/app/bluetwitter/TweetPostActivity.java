package com.matto1990.app.bluetwitter;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;

public class TweetPostActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_post);
        
        Uri uri = getIntent().getData();
        String msg = uri.getQueryParameter("msg");
        
        ((EditText) findViewById(R.id.txt_tweet)).setText(msg);
    }
}