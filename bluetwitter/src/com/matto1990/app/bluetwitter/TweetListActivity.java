package com.matto1990.app.bluetwitter;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

public class TweetListActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tweets);
        
        Uri uri = getIntent().getData();
        String user = uri.getQueryParameter("user");
        
        ((TextView) findViewById(R.id.txt_tweets)).setText("Tweets for " + user);
    }
}