package com.matto1990.app.appusingtwitter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class HomeActivity extends Activity {
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    
    findViewById(R.id.btn_launch).setOnClickListener(launch);
    findViewById(R.id.btn_post_madlab).setOnClickListener(postMadlab);
    findViewById(R.id.btn_post_androidmcr).setOnClickListener(postAndroidmcr);
  }
  
  private OnClickListener launch = new OnClickListener() {
    @Override
    public void onClick(View view) {
      String user = ((EditText) findViewById(R.id.edit_user)).getText().toString();
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse("twitter://tweets?user=" + user));
      startActivity(intent);
    }
  };
  
  private OnClickListener postMadlab = new OnClickListener() {
    @Override
    public void onClick(View view) {
      postFromTwitter("Hello from @Madlabuk!");
    }
  };
  
  private OnClickListener postAndroidmcr = new OnClickListener() {
    @Override
    public void onClick(View view) {
      postFromTwitter("I'm at @android_mcr!");
    }
  };

  protected void postFromTwitter(String msg) {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse("twitter://post?msg=" + msg));
    startActivity(intent);
  }
}